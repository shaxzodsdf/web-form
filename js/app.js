document.addEventListener("DOMContentLoaded", function () {
    formInteractive()
    signIn()
});

function signIn(){
    const formLog = document.getElementById('login-form')

    formLog.addEventListener('submit', function (event) {
        event.preventDefault();

        const email = formLog.email.value;
        const pass = formLog.password.value;

        if (isValidEmail(email) && isValidPassword(pass)){
            location.reload();
        }else {
            alert("Check login or password")
        }

    });
}
function formInteractive() {
    const mailLog = document.getElementById('email')
    const passwordLog = document.getElementById('password')

    mailLog.addEventListener("input", () => {
        if (isValidEmail(mailLog.value)) {
            mailLog.classList.remove("input-error")
        } else {
            mailLog.classList.add("input-error")
        }
    })
    passwordLog.addEventListener("input", () => {
        if (isValidPassword(passwordLog.value)) {
            passwordLog.classList.remove("input-error")
        } else {
            passwordLog.classList.add("input-error")
        }
    })
}
function isValidEmail(str) {
    // Your code
    if (!str || typeof str === 'undefined') {
        return false;
    } else {
        return /^[\w-]+@([\w-]+\.)+[\w-]{2,4}$/.test(str);
    }
}

function isValidPassword(str) {
    // Your code
    if (!str || typeof str === 'undefined') {
        return false;
    } else {
        //Minimum eight characters, at least one uppercase letter, one lowercase letter and one number
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(str);
    }
}